const std = @import("std");

const Subcommand = enum {
    help,
    @"--help",
    @"-h",
    init,
    @"cat-file",
    @"hash-object",
    @"ls-tree",
};

pub fn main() !void {
    var gpa_impl = std.heap.GeneralPurposeAllocator(.{}){};
    const gpa = gpa_impl.allocator();
    var it = try std.process.ArgIterator.initWithAllocator(gpa);
    defer it.deinit();

    _ = it.skip();

    const subcommand = subcommand: {
        const subc_string = it.next() orelse printHelp();

        break :subcommand std.meta.stringToEnum(Subcommand, subc_string) orelse {
            std.debug.print("invalid subcommand. I got: {s} \n", .{subc_string});
            try printHelp();
        };
    };
    switch (subcommand) {
        .help, .@"--help", .@"-h" => printHelp(),
        .init => {
            initGitRepo(gpa, &it) catch |err| {
                std.debug.print("Caught an error: {}\n", .{err});
            };
        },
        .@"cat-file" => try readGlob(gpa, &it),
        .@"hash-object" => try writeGlob(gpa, &it),
        .@"ls-tree" => try readTree(gpa, &it),
    }
}

fn initGitRepo(allocator: std.mem.Allocator, it: *std.process.ArgIterator) !void {
    const initDirs =
        \\.git
        \\.git/objects
        \\.git/refs
    .*;

    var dir_it = std.mem.tokenizeAny(u8, &initDirs, "\n");
    const path = it.next() orelse ".";
    if (std.mem.eql(u8, path, "--help") or std.mem.eql(u8, path, "-h")) {
        std.debug.print(
            \\zigit init is a crappy implementation of git init.
            \\
            \\Usage:
            \\  zigit init
            \\  zigit init /path/to/dir
            \\  zigit init path/to/dir
            \\
        , .{});
        std.process.exit(1);
    }
    std.fs.cwd().makeDir(path) catch |err| {
        if (err != error.PathAlreadyExists) {
            return err;
        }
    };
    while (dir_it.next()) |dir| {
        const buf = try std.fmt.allocPrint(allocator, "{s}/{s}", .{ path, dir });
        defer allocator.free(buf);

        std.fs.cwd().makeDir(buf) catch |err| {
            switch (err) {
                error.PathAlreadyExists => {
                    std.debug.print("{s} already exists.\n", .{buf});
                },
                else => return err,
            }
        };
    }
    const buf = try std.fmt.allocPrint(allocator, "{s}/{s}", .{ path, ".git/HEAD" });
    defer allocator.free(buf);

    const writeOptions: std.fs.Dir.WriteFileOptions = .{
        .sub_path = buf,
        .data = "ref: refs/heads/main\n",
    };
    std.fs.cwd().writeFile(writeOptions) catch |err| {
        std.debug.print("Got an error: {}", .{err});
        return err;
    };
}

fn readGlob(allocator: std.mem.Allocator, it: *std.process.ArgIterator) !void {
    const flag = flag: {
        const flag_string = it.next() orelse {
            std.debug.print("missing parameters for pretty print and glob\n", .{});
            readGlobHelp();
        };
        break :flag (std.mem.eql(u8, flag_string, "-p"));
    };
    if (flag) {
        const glob = glob: {
            const glob_string = it.next() orelse {
                std.debug.print("missing parameters for pretty print and glob\n", .{});
                readGlobHelp();
            };
            if (glob_string.len != 40) {
                std.debug.print("Not a glob. len: {d}\n", .{glob_string.len});
                readGlobHelp();
            }
            break :glob glob_string;
        };
        const folder = glob[0..2];
        const file = glob[2..];
        // TODO: find "../../.git" somehow?
        // Maybe just going up a directory until I find one.
        // Maybe set a default depth to only go say 4.
        // could also make $HOME the limit.
        // Guess the problem becomes...How do I know I know I've hit $HOME.
        // NOTE: .git/objects/ is [13]u8 {folder}/{file} is [41]u8
        // 13 + 41 == 54; Should only need to allocate 54 bytes.
        // would like to be able to find the .git folder similarly to how git does.
        const path = try std.fmt.allocPrint(allocator, ".git/objects/{s}/{s}", .{ folder, file });
        defer allocator.free(path);

        const f = try std.fs.cwd().openFile(path, .{ .mode = .read_only });
        defer f.close();

        var out = std.ArrayList(u8).init(allocator);
        defer out.deinit();

        var dcp = std.compress.zlib.decompressor(f.reader());
        try dcp.reader().readAllArrayList(&out, std.math.maxInt(usize));
        if (std.mem.startsWith(u8, out.items, "blob")) {
            var split = std.mem.splitAny(u8, out.items, "\x00");
            _ = split.next().?;
            const stdOut = std.io.getStdOut();
            _ = try stdOut.write(split.next().?);
        } else {
            std.debug.print("Not supported... Found tree, commit, or tag.\n", .{});
            readGlobHelp();
        }
    } else {
        std.debug.print("missing parameters for pretty print and glob\n", .{});
        readGlobHelp();
    }
}

fn readGlobHelp() noreturn {
    std.debug.print(
        \\zigit cat-file requires a flag.
        \\cannot read trees, commits, or tags. yet.
        \\Available flags: 
        \\  -p
        \\
        \\Note:
        \\  <glob> length has to be 40.
        \\
        \\Examples:
        \\  zigit cat-file -p <SHA>
        \\
    , .{});
    std.process.exit(1);
}

fn writeGlob(allocator: std.mem.Allocator, it: *std.process.ArgIterator) !void {
    const flag = flag: {
        const flag_string = it.next() orelse {
            std.debug.print("missing parameters for pretty print and glob\n", .{});
            writeGlobHelp();
        };
        break :flag (std.mem.eql(u8, flag_string, "-w"));
    };
    if (flag) {
        const path = glob: {
            const filePath = it.next() orelse {
                std.debug.print("missing parameters for pretty print and glob\n", .{});
                writeGlobHelp();
            };
            break :glob filePath;
        };
        var input_file = try std.fs.cwd().openFile(path, .{ .mode = .read_only });
        defer input_file.close();
        var metadata = try input_file.metadata();
        const contents = try input_file.reader().readAllAlloc(allocator, std.math.maxInt(usize));

        const header = try std.fmt.allocPrint(allocator, "blob {d}\x00", .{metadata.size()});
        defer allocator.free(header);

        var blobArray = std.ArrayList([]const u8).init(allocator);
        defer blobArray.deinit();
        try blobArray.append(header);
        try blobArray.append(contents);

        var hash = std.crypto.hash.Sha1.init(.{});
        for (blobArray.items) |item| {
            hash.update(item);
        }
        var buf: [20]u8 = undefined;
        hash.final(&buf);
        const hex_buf = std.fmt.bytesToHex(buf, .lower);

        const directoryPath = try std.fmt.allocPrint(allocator, ".git/objects/{s}", .{hex_buf[0..2]});
        defer allocator.free(directoryPath);
        _ = std.fs.cwd().makeDir(directoryPath) catch |err| {
            if (err != error.PathAlreadyExists) {
                return err;
            }
        };

        const globFile = try std.fmt.allocPrint(allocator, ".git/objects/{s}/{s}", .{ hex_buf[0..2], hex_buf[2..] });
        defer allocator.free(globFile);
        const output_file = try std.fs.cwd().createFile(globFile, .{});
        defer output_file.close();
        var compress = try std.compress.zlib.compressor(output_file.writer(), .{});
        for (blobArray.items) |item| {
            var bufReader = std.io.fixedBufferStream(item);
            try compress.compress(bufReader.reader());
        }
        try compress.finish();
    }
}

fn writeGlobHelp() noreturn {
    std.debug.print(
        \\zigit hash-object requires a flag.
        \\only writes blobs.
        \\Required flags: 
        \\  -w
        \\
        \\Examples:
        \\  zigit hash-object -w /path/to/file
        \\
    , .{});
    std.process.exit(1);
}

fn readTree(allocator: std.mem.Allocator, it: *std.process.ArgIterator) !void {
    _ = allocator;
    const flag = flag: {
        const flag_string = it.next() orelse {
            std.debug.print("missing --name-only\n", .{});
            printLsTreeHelp();
        };
        break :flag (std.mem.eql(u8, flag_string, "--name-only"));
    };
    if (flag) {} else {
        printLsTreeHelp();
    }
}

fn printLsTreeHelp() noreturn {
    std.debug.print(
        \\zigit ls-tree is a crappy ls-tree reader.
        \\
        \\required flags: --name-only
        \\
        \\Examples:
        \\  zigit ls-tree --name-only <SHA>
        \\
        \\
        \\  use `zigit <command> --help` to get subcommand-specific information
        \\
    , .{});
    std.process.exit(1);
}

fn printHelp() noreturn {
    std.debug.print(
        \\zigit is a crappy git implementation.
        \\
        \\Available commands: init, hash-object, ls-tree, cat-file
        \\
        \\Examples:
        \\  zigit init
        \\  zigit init /path/to/dir
        \\  zigit hash-object -w /path/to/file
        \\  zigit ls-tree --name-only <SHA>
        \\  zigit cat-file -p <SHA>
        \\
        \\  use `zigit <command> --help` to get subcommand-specific information
        \\
    , .{});
    std.process.exit(1);
}
