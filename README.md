# zigit
using [this](https://app.codecrafters.io/courses/git/introduction) as a reference for implementing git. 
Currently only implemented the init function and cat-file for blobs...
## requirements
- zig, 0.13.0, as of writing.
## references
- [Codecrafters](https://app.codecrafters.io/courses/git/introduction)
- [What is in that .git directory](https://blog.meain.io/2023/what-is-in-dot-git/)
- [Git Objects](https://git-scm.com/book/en/v2/Git-Internals-Git-Objects)
